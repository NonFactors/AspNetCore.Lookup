namespace NonFactors.Mvc.Lookup;

public class TestLookup<T> : ALookup<T> where T : class
{
    public IList<T> Models { get; }

    public TestLookup()
    {
        Models = new List<T>();
    }

    public override IQueryable<T> GetModels()
    {
        return Models.AsQueryable();
    }
}
