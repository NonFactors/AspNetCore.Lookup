namespace NonFactors.Mvc.Lookup;

public class Int32Model
{
    [Key]
    [LookupColumn]
    public Int32 Value { get; set; }
}
