namespace NonFactors.Mvc.Lookup;

public class ObjectModel
{
    [Key]
    public Object? Id { get; set; }
}
