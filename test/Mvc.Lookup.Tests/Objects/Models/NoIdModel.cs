namespace NonFactors.Mvc.Lookup;

public class NoIdModel
{
    [LookupColumn]
    public String? Title { get; set; }
}
