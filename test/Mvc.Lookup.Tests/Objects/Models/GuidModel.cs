namespace NonFactors.Mvc.Lookup;

public class GuidModel
{
    [LookupColumn]
    public Guid Id { get; set; }
}
