namespace NonFactors.Mvc.Lookup;

public class LookupQueryTests
{
    [Fact]
    public void IsOrdered_False()
    {
        Assert.False(LookupQuery.IsOrdered(Array.Empty<Object>().OrderBy(_ => 0).AsQueryable()));
    }

    [Fact]
    public void IsOrdered_True()
    {
        Assert.True(LookupQuery.IsOrdered(Array.Empty<Object>().AsQueryable().OrderBy(_ => 0)));
    }
}
