namespace NonFactors.Mvc.Lookup;

public class LookupFilterTests
{
    [Fact]
    public void LookupFilter_CreatesEmpty()
    {
        LookupFilter filter = new();

        Assert.Empty(filter.AdditionalFilters);
        Assert.Empty(filter.Selected);
        Assert.Empty(filter.CheckIds);
        Assert.Empty(filter.Ids);
    }
}
