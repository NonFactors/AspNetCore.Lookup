namespace NonFactors.Mvc.Lookup;

public class LookupColumnTests
{
    [Fact]
    public void LookupColumn_Defaults()
    {
        LookupColumn actual = new("Test", "Headers");

        Assert.Equal("Headers", actual.Header);
        Assert.Equal("Test", actual.Key);
        Assert.Empty(actual.CssClass);
        Assert.False(actual.Hidden);
    }
}
