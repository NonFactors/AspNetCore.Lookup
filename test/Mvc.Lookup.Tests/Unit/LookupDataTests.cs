namespace NonFactors.Mvc.Lookup;

public class LookupDataTests
{
    [Fact]
    public void LookupData_CreatesEmpty()
    {
        LookupData actual = new();

        Assert.Empty(actual.Selected);
        Assert.Empty(actual.Columns);
        Assert.Empty(actual.Rows);
    }
}
