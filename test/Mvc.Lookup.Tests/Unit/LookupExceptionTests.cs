namespace NonFactors.Mvc.Lookup;

public class LookupExceptionTests
{
    [Fact]
    public void LookupException_Message()
    {
        Assert.Equal("Test", new LookupException("Test").Message);
    }
}
