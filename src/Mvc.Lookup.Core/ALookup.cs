namespace NonFactors.Mvc.Lookup;

public abstract class ALookup
{
    public LookupFilterCase FilterCase { get; set; }
    public LookupFilterMethod FilterMethod { get; set; }

    public LookupFilter Filter { get; set; }
    public IList<LookupColumn> Columns { get; set; }
    public IList<String> AdditionalFilters { get; set; }

    protected ALookup()
    {
        FilterMethod = LookupFilterMethod.Contains;
        AdditionalFilters = new List<String>();
        FilterCase = LookupFilterCase.Original;
        Columns = new List<LookupColumn>();
        Filter = new LookupFilter();
    }

    public virtual String GetColumnKey(PropertyInfo property)
    {
        return property.Name;
    }
    public virtual String GetColumnHeader(PropertyInfo property)
    {
        return property.GetCustomAttribute<DisplayAttribute>(false)?.GetShortName() ?? "";
    }
    public virtual String GetColumnCssClass(PropertyInfo property)
    {
        return "";
    }

    public abstract LookupData GetData();
}
