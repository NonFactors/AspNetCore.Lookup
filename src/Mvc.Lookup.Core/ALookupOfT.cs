namespace NonFactors.Mvc.Lookup;

public abstract class ALookup<T> : ALookup where T : class
{
    public Func<T, String> GetId { get; set; }
    public Func<T, String> GetLabel { get; set; }
    public virtual IEnumerable<PropertyInfo> AttributedProperties
    {
        get
        {
            return typeof(T)
                .GetProperties()
                .Where(property => property.IsDefined(typeof(LookupColumnAttribute), false))
                .OrderBy(property => property.GetCustomAttribute<LookupColumnAttribute>(false)!.Position);
        }
    }

    protected ALookup()
    {
        GetLabel = model => GetValue(model, Columns.Where(col => !col.Hidden).Select(col => col.Key).FirstOrDefault() ?? "") ?? "";
        String key = Array.Find(typeof(T).GetProperties(), prop => prop.IsDefined(typeof(KeyAttribute)))?.Name ?? "Id";
        GetId = model => GetValue(model, key) ?? "";

        foreach (PropertyInfo property in AttributedProperties)
        {
            LookupColumnAttribute column = property.GetCustomAttribute<LookupColumnAttribute>(false)!;
            Columns.Add(new LookupColumn(GetColumnKey(property), GetColumnHeader(property))
            {
                CssClass = GetColumnCssClass(property),
                FilterMethod = column.FilterMethod,
                FilterCase = column.FilterCase,
                Filterable = column.Filterable,
                Hidden = column.Hidden
            });
        }
    }

    public override LookupData GetData()
    {
        IQueryable<T> models = GetModels();
        IQueryable<T> selected = Array.Empty<T>().AsQueryable();
        IQueryable<T> notSelected = Sort(FilterByRequest(models));

        if (Filter.Offset == 0 && Filter.Ids.Count == 0 && Filter.Selected.Count > 0)
            selected = Sort(FilterBySelected(models, Filter.Selected));

        return FormLookupData(notSelected, selected, Page(notSelected));
    }
    public abstract IQueryable<T> GetModels();

    private IQueryable<T> FilterByRequest(IQueryable<T> models)
    {
        if (Filter.Ids.Count > 0)
            return FilterByIds(models, Filter.Ids);

        if (Filter.Selected.Count > 0)
            models = FilterByNotIds(models, Filter.Selected);

        if (Filter.CheckIds.Count > 0)
            models = FilterByCheckIds(models, Filter.CheckIds);

        if (Filter.AdditionalFilters.Count > 0)
            models = FilterByAdditionalFilters(models);

        return FilterBySearch(models);
    }
    public virtual IQueryable<T> FilterBySearch(IQueryable<T> models)
    {
        if (String.IsNullOrEmpty(Filter.Search))
            return models;

        List<Object> values = new();
        List<String> queries = new();

        foreach (LookupColumn column in Columns.Where(column => !column.Hidden && column.Filterable))
        {
            PropertyInfo? property = typeof(T).GetProperty(column.Key);

            if (GenerateQuery(column, property, queries.Count) is String query && ParseSearch(column, property) is Object value)
            {
                queries.Add(query);
                values.Add(value);
            }
        }

        return queries.Count == 0 ? models : models.Where(String.Join(" || ", queries), values.ToArray());
    }
    public virtual IQueryable<T> FilterByAdditionalFilters(IQueryable<T> models)
    {
        foreach ((String? property, Object? value) in Filter.AdditionalFilters.Where(item => item.Value != null && typeof(T).GetProperty(item.Key) != null))
            if (value is IEnumerable and not String)
                models = models.Where($"@0.Contains({property})", value);
            else
                models = models.Where($"({property} != null && {property} == @0)", value);

        return models;
    }
    public virtual IQueryable<T> FilterByIds(IQueryable<T> models, IList<String> ids)
    {
        PropertyInfo? key = Array.Find(typeof(T).GetProperties(), prop => prop.IsDefined(typeof(KeyAttribute))) ?? typeof(T).GetProperty("Id");

        if (key == null)
            throw new LookupException($"'{typeof(T).Name}' type does not have key or property named 'Id', required for automatic id filtering.");

        if (!IsFilterable(key.PropertyType))
            throw new LookupException($"'{typeof(T).Name}.{key.Name}' property type is not filterable.");

        return models.Where($"@0.Contains({key.Name})", Parse(key.PropertyType, ids));
    }
    public virtual IQueryable<T> FilterByNotIds(IQueryable<T> models, IList<String> ids)
    {
        PropertyInfo? key = Array.Find(typeof(T).GetProperties(), prop => prop.IsDefined(typeof(KeyAttribute))) ?? typeof(T).GetProperty("Id");

        if (key == null)
            throw new LookupException($"'{typeof(T).Name}' type does not have key or property named 'Id', required for automatic id filtering.");

        if (!IsFilterable(key.PropertyType))
            throw new LookupException($"'{typeof(T).Name}.{key.Name}' property type is not filterable.");

        return models.Where($"!@0.Contains({key.Name})", Parse(key.PropertyType, ids));
    }
    public virtual IQueryable<T> FilterByCheckIds(IQueryable<T> models, IList<String> ids)
    {
        return FilterByIds(models, ids);
    }
    public virtual IQueryable<T> FilterBySelected(IQueryable<T> models, IList<String> ids)
    {
        return FilterByIds(models, ids);
    }

    public virtual IQueryable<T> Sort(IQueryable<T> models)
    {
        if (!String.IsNullOrWhiteSpace(Filter.Sort))
            return models.OrderBy(Filter.Sort + " " + Filter.Order);

        if (LookupQuery.IsOrdered(models))
            return models;

        return models.OrderBy(_ => 0);
    }

    public virtual IQueryable<T> Page(IQueryable<T> models)
    {
        Filter.Offset = Math.Max(0, Filter.Offset);
        Filter.Rows = Math.Max(1, Math.Min(Filter.Rows, 100));

        return models.Skip(Filter.Offset).Take(Filter.Rows);
    }

    public virtual LookupData FormLookupData(IQueryable<T> filtered, IQueryable<T> selected, IQueryable<T> notSelected)
    {
        LookupData data = new();
        data.Columns = Columns;

        foreach (T model in selected)
            data.Selected.Add(FormData(model));

        foreach (T model in notSelected)
            data.Rows.Add(FormData(model));

        return data;
    }

    public virtual Dictionary<String, String?> FormData(T model)
    {
        Dictionary<String, String?> data = new();

        foreach (LookupColumn column in Columns)
            data[column.Key] = GetValue(model, column.Key);

        data["Label"] = GetLabel(model);
        data["Id"] = GetId(model);

        return data;
    }

    private String? GenerateQuery(LookupColumn column, PropertyInfo? property, Int32 index)
    {
        if (property == null)
            return null;

        if (property.PropertyType == typeof(String))
        {
            LookupFilterMethod method = column.FilterMethod ?? FilterMethod;

            return $"({column.Key} != null && {ConvertCase(column)}.{method}(@{index}))";
        }

        return IsFilterable(property.PropertyType) ? $"{column.Key} = @{index}" : null;
    }
    private Object? ParseSearch(LookupColumn column, PropertyInfo? property)
    {
        if (property == null)
            return null;

        if (property.PropertyType == typeof(String))
        {
            LookupFilterCase filterCase = column.FilterCase ?? FilterCase;

            return filterCase switch
            {
                LookupFilterCase.Upper => Filter.Search?.ToUpper(),
                LookupFilterCase.Lower => Filter.Search?.ToLower(),
                _ => Filter.Search
            };
        }

        try
        {
            return TypeDescriptor.GetConverter(property.PropertyType).ConvertFrom(Filter.Search!);
        }
        catch
        {
            return null;
        }
    }
    private String? GetValue(T model, String propertyName)
    {
        PropertyInfo? property = typeof(T).GetProperty(propertyName);

        if (property == null)
            return null;

        if (property.GetCustomAttribute<LookupColumnAttribute>(false) is LookupColumnAttribute { Format: { } } column)
            return String.Format(column.Format, property.GetValue(model));

        return property.GetValue(model)?.ToString();
    }
    private Object Parse(Type type, IList<String> ids)
    {
        TypeConverter converter = TypeDescriptor.GetConverter(type);
        IList values = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(type))!;

        foreach (String value in ids)
            values.Add(converter.ConvertFrom(value));

        return values;
    }
    private String ConvertCase(LookupColumn column)
    {
        return (column.FilterCase ?? FilterCase) switch
        {
            LookupFilterCase.Upper => $"{column.Key}.ToUpper()",
            LookupFilterCase.Lower => $"{column.Key}.ToLower()",
            _ => column.Key
        };
    }
    private Boolean IsFilterable(Type type)
    {
        type = Nullable.GetUnderlyingType(type) ?? type;

        switch (Type.GetTypeCode(type))
        {
            case TypeCode.SByte:
            case TypeCode.Byte:
            case TypeCode.Int16:
            case TypeCode.UInt16:
            case TypeCode.Int32:
            case TypeCode.UInt32:
            case TypeCode.Int64:
            case TypeCode.UInt64:
            case TypeCode.Single:
            case TypeCode.Double:
            case TypeCode.Decimal:
            case TypeCode.String:
            case TypeCode.DateTime:
                return true;
            default:
                return type == typeof(Guid);
        }
    }
}
