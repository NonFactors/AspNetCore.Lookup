using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace NonFactors.Mvc.Lookup;

[HtmlTargetElement("div", Attributes = "mvc-lookup,url")]
[HtmlTargetElement("div", Attributes = "mvc-lookup-for,url")]
public class LookupTagHelper : TagHelper
{
    public String? Url { get; set; }
    public Int32? Rows { get; set; }
    public String? Name { get; set; }
    public String? Sort { get; set; }
    public Object? Value { get; set; }
    public String? Title { get; set; }
    public String? Search { get; set; }
    public String? Dialog { get; set; }
    public String? Filters { get; set; }
    public Boolean? Multi { get; set; }
    public Boolean? Browser { get; set; }
    public Boolean? Readonly { get; set; }
    public String? Placeholder { get; set; }

    [HtmlAttributeName("order")]
    public LookupSortOrder? SortOrder { get; set; }

    [HtmlAttributeName("add-handler")]
    public Boolean? AddHandler { get; set; }

    [HtmlAttributeName("mvc-lookup")]
    public String? LookupName { get; set; }

    [HtmlAttributeName("mvc-lookup-for")]
    public ModelExpression? Lookup { get; set; }

    [ViewContext]
    [HtmlAttributeNotBound]
    public ViewContext ViewContext { get; set; }

    private IHtmlGenerator Html { get; }
    private Func<ViewContext, IUrlHelper> UrlFactory { get; }

    public LookupTagHelper(IHtmlGenerator html, IUrlHelperFactory factory)
    {
        Html = html;
        ViewContext = null!;
        UrlFactory = factory.GetUrlHelper;
    }

    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
        Type type = Lookup?.Metadata.ModelType ?? Value?.GetType() ?? typeof(Object);

        Url = Url?.StartsWith("~") == true ? UrlFactory(ViewContext).Content(Url) : Url;
        Multi ??= typeof(IEnumerable).IsAssignableFrom(type) && type != typeof(String);
        LookupName ??= Lookup?.Name;
        Value ??= Lookup?.Model;

        WriteAttributes(output);
        WriteValues(output);
        WriteControl(output);

        if (Browser != false)
            WriteBrowser(output);
    }

    private void WriteValues(TagHelperOutput output)
    {
        Dictionary<String, Object?> attributes = new()
        {
            ["class"] = "mvc-lookup-value",
            ["autocomplete"] = "off"
        };

        TagBuilder container = new("div");
        container.AddCssClass("mvc-lookup-values");
        container.Attributes["data-for"] = LookupName;

        if (Multi == true)
        {
            foreach (Object val in (Value as IEnumerable)?.Cast<Object>() ?? Array.Empty<Object>())
            {
                TagBuilder input = new("input");
                input.Attributes["value"] = Html.FormatValue(val, null);
                input.TagRenderMode = TagRenderMode.SelfClosing;
                input.Attributes["name"] = LookupName;
                input.Attributes["type"] = "hidden";
                input.MergeAttributes(attributes);

                container.InnerHtml.AppendHtml(input);
            }
        }
        else
        {
            container.InnerHtml.AppendHtml(Html.GenerateHidden(ViewContext, Lookup?.ModelExplorer, LookupName, Value, Lookup?.ModelExplorer == null && Value == null, attributes));
        }

        output.Content.AppendHtml(container);
    }
    private void WriteControl(TagHelperOutput output)
    {
        TagBuilder search = new("input") { TagRenderMode = TagRenderMode.SelfClosing };
        Dictionary<String, Object?> attributes = new();
        TagBuilder control = new("div");
        TagBuilder loader = new("div");
        TagBuilder error = new("div");

        attributes["class"] = "mvc-lookup-input";
        attributes["autocomplete"] = "off";

        if (Placeholder != null)
            attributes["placeholder"] = Placeholder;

        if (Readonly == true)
            attributes["readonly"] = "readonly";

        if (Name != null)
            attributes["name"] = Name;

        loader.AddCssClass("mvc-lookup-control-loader");
        error.AddCssClass("mvc-lookup-control-error");
        control.Attributes["data-for"] = LookupName;
        control.AddCssClass("mvc-lookup-control");
        search.MergeAttributes(attributes);
        error.InnerHtml.Append("!");

        control.InnerHtml.AppendHtml(Name == null ? search : Html.GenerateTextBox(ViewContext, null, Name, null, null, attributes));
        control.InnerHtml.AppendHtml(loader);
        control.InnerHtml.AppendHtml(error);

        output.Content.AppendHtml(control);
    }
    private void WriteBrowser(TagHelperOutput output)
    {
        TagBuilder browser = new("button");
        browser.Attributes["type"] = "button";
        browser.Attributes["tabindex"] = "-1";
        browser.AddCssClass("mvc-lookup-browser");
        browser.Attributes["data-for"] = LookupName;

        TagBuilder icon = new("span");
        icon.AddCssClass("mvc-lookup-icon");

        browser.InnerHtml.AppendHtml(icon);

        output.Content.AppendHtml(browser);
    }
    private void WriteAttributes(TagHelperOutput output)
    {
        String classes = "mvc-lookup";

        if (Readonly == true)
            classes += " mvc-lookup-readonly";

        if (Browser == false)
            classes += " mvc-lookup-browseless";

        WriteAttribute(output, "data-url", Url);
        WriteAttribute(output, "data-rows", Rows);
        WriteAttribute(output, "data-sort", Sort);
        WriteAttribute(output, "data-multi", Multi);
        WriteAttribute(output, "data-title", Title);
        WriteAttribute(output, "data-dialog", Dialog);
        WriteAttribute(output, "data-search", Search);
        WriteAttribute(output, "data-for", LookupName);
        WriteAttribute(output, "data-order", SortOrder);
        WriteAttribute(output, "data-filters", Filters);
        WriteAttribute(output, "data-readonly", Readonly);
        WriteAttribute(output, "data-add-handler", AddHandler);
        WriteAttribute(output, "class", (classes + " " + output.Attributes["class"]?.Value).Trim());
    }
    private void WriteAttribute(TagHelperOutput output, String key, Object? value)
    {
        if (value != null)
            output.Attributes.SetAttribute(key, value);
    }
}
